<?php
namespace PlusB\AjaxForm\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "PlusB.IM".              *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Contact {

    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     *
     */
    protected $name;

    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     *
     */
    protected $organisation;

    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     *
     */
    protected $email;

    /**
     * @var string
     * @Flow\Validate(type="NotEmpty")
     *
     */
    protected $message;

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getOrganisation() {
        return $this->organisation;
    }

    /**
     * @param string $organisation
     */
    public function setOrganisation($organisation) {
        $this->organisation = $organisation;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message) {
        $this->message = $message;
    }
}