<?php
namespace PlusB\AjaxForm\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "PlusB.AjaxForm".        *
 *                                                                        *
 *                                                                        */

use PlusB\AjaxForm\Domain\Model\Contact;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Error\Result;
use TYPO3\Flow\Http\Headers;
use TYPO3\Flow\Http\Response;

class FormController extends \TYPO3\Flow\Mvc\Controller\ActionController {

	/**
	 * These are the expected media types sent by the request. With this, it is possible to e.g.
	 * implement REST-interfaces
	 *
	 * @var array
	 */
	protected $supportedMediaTypes = array(
		'text/html',
		'application/json'
	);

	/**
	 * Just show the form
	 *
	 * @return void
	 */
	public function showAction() {
		$contact = new Contact();
		$this->view->assign('contact', $contact);

		/**
		 *
		 * First, we want to show the plugin output within the rest of neos page content (header, body, and so on).
		 * But when the form has been sent and there was a validation error, we just want to send the html output of the
		 * plugin back to the client
		 *
		 */

		$validationResultsArgumentName = '__submittedArgumentValidationResults';
		/** @var $validationResults Result */
		$validationResults = $this->request->getInternalArgument($validationResultsArgumentName);
		if($validationResults !== NULL && $validationResults->hasErrors()){
			$this->sendResponse();
		}
	}

	/**
	 *
	 * The form has been sent and succesfully validated. We want to show a confirmation message
	 *
	 * @param Contact $contact
	 */
	public function sendAction(Contact $contact){
		$this->view->assign('confirmationMessage', $this->settings['Form']['confirmationMessage']);
		$this->sendResponse();
	}

	/**
	 *
	 * Send the pure plugin ouput back to the client, without the head and body tags and stuff.
	 * You could easily extend this actioncontroller to respond with a pure json string
	 *
	 */
	private function sendResponse(){
		ob_clean();
		$this->response->setHeaders(new Headers(array('Content-Type' => 'text/html')));
		$this->response->setContent($this->view->render());
		$this->response->send();
		exit;
	}

}